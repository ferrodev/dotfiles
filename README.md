# Evan's dotfiles

Use at your own risk.

## Contents

- editorconfig
- PowerShell config

## PowerShell setup (Windows)

- [Scoop](https://scoop.sh) - a command-line installer
- [Git for Windows](https://gitforwindows.org)
- [Oh My Posh](https://ohmyposh.dev) - prompt theme engine
- [Terminal Icons](https://github.com/devblackops/Terminal-Icons) - folder and file icons
- [PSReadLine](https://docs.microsoft.com/en-us/powershell/module/psreadline) - cmdlet for customizing the editing environment, used for autocompletion
- [z](https://www.powershellgallery.com/packages/z/) - directory jumper

